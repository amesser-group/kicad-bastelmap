##########################
Bastelmap extras for KiCad
##########################

This project contains additional footprints, symbols
and maybe more to be used with `KiCad`_. I need thes definitions
for my projects.

.. _KiCad: https://www.kicad.org


Licensing
=========

All files of the project are licensed under the conditions of 
CC BY-SA license. Software files are covered by GPL v3 if
not otherwise noted in the files


Copyright
=========

If not otherwise noted within the files, the copyright for 
all files in this project folder is
(c) 2022 Andreas Messer <andi@bastelmap.de>.

Links
=====

.. target-notes::


