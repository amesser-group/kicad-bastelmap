#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from __future__ import division
import pcbnew

import FootprintWizardBase
import PadArray as PA
import math

class EurorackFrontplateWizard(FootprintWizardBase.FootprintWizard):
    ''' Generates Döpfer A100 Eurorack Front Plate footprint

        See https://doepfer.de/a100_man/a100m_d.htm for mechanical 
        definitions
    '''

    def GetName(self):
        return "EurorackFrontplate"

    def GetDescription(self):
        return "Eurorack Frontplate Wizard"

    def GenerateParameterList(self):
        self.AddParam("Size",  "HP", self.uInteger, 8, designator='hp')
        self.AddParam("Mounting Holes", "Count", self.uInteger, 2 , designator='mh_count')

    def CheckParameters(self):
        pass


    def GetValue(self):
        hp = (self.parameters["Size"]["HP"])

        return f"A100_{hp}HP"

    ''' HP dependent width definitions from Döpfer A100 Eurorack definitions
    
    '''
    width = {
        1  :   5.0,
        2  :   9.8,
        4  :  20.00,
        6  :  30.00,
        8  :  40.30,
        10 :  50.50,
        12 :  60.60,
        14 :  70.80,
        16 :  80.90,
        18 :  91.30,
        20 : 101.30
    }

    def BuildThisFootprint(self):
        hp = (self.parameters["Size"]["HP"])
        mnth_max_cnt = (self.parameters["Mounting Holes"]["Count"])


        height = pcbnew.FromMM(128.5)
        width  = pcbnew.FromMM(self.width[hp])

        w_half = width / 2.
        h_half = height / 2.


        # Add mounting holes
        drill = pcbnew.FromMM(3.2)

        mnth = pcbnew.PAD(self.module)
        mnth.SetSize(pcbnew.wxSize(drill, drill))
        mnth.SetShape(pcbnew.PAD_SHAPE_CIRCLE)
        mnth.SetAttribute(pcbnew.PAD_ATTRIB_NPTH)
        mnth.SetLayerSet(mnth.UnplatedHoleMask())
        mnth.SetDrillSize(pcbnew.wxSize(drill, drill))
        mnth.SetOrientation(0)

        # mounting holes are offset by 5.08 mm units
        mnth_dist      = pcbnew.FromMM(5.08)
        mnth_min_space = pcbnew.FromMM(5.5) / 2

        h6_half = pcbnew.FromMM(122.5) / 2

        # make a list of all possible mounting holes
        # we allow an extra hole at offset 2.46 to allow for very thin plates
        mnth_cnt = math.ceil((width - mnth_min_space - pcbnew.FromMM(7.50) + mnth_dist) / mnth_dist)
        mnth_pos = [pcbnew.FromMM(7.50) - mnth_dist - w_half + mnth_dist * x for x in range(mnth_cnt)]

        # if we have more than 1 hole, remove the first one at 2.46 offset again
        if len(mnth_pos) > 1:
            mnth_pos = mnth_pos[1:]

        # reduce holes to request amount evenly spaced
        if len(mnth_pos) > mnth_max_cnt:
            interval  = math.ceil((len(mnth_pos) - 1) / (mnth_max_cnt - 1))

            mnth_pos = [mnth_pos[0]] + mnth_pos[interval:-1:interval] + [mnth_pos[-1]]

        mounting_holes = []
        for x in mnth_pos:
            mounting_holes.extend([(x, -h6_half), (x, h6_half)])

        array = PA.PadCustomArray(mnth, mounting_holes)
        array.AddPadsToModule(self.draw)

        # Draw outline of plate
        self.draw.SetLayer(pcbnew.F_Fab)

        self.draw.SetLineThickness( pcbnew.FromMM( 0.1 ) ) #Default per KLC F5.2 as of 12/2018
        self.draw.Box(0, 0, width, height)

        # Draw Eurorack Case space. While a typical 3HE Eurorack PCB is 100m height,
        # There is actually more space within case available. This can be used to align 
        # Pots or switches beter
        # H5 >= 112mm 
        h5_half = pcbnew.FromMM(112) / 2
        self.draw.Line(-w_half, -h5_half, w_half, -h5_half)
        self.draw.Line(-w_half,  h5_half, w_half, h5_half )


EurorackFrontplateWizard().register()
